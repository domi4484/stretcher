#include "AndroidHelper.h"

#include <QDebug>

//-------------------------------------------------------------------------------------------------

AndroidHelper::AndroidHelper()
  : m_QAndroidJniObject_WakeLock()
{
  // Create wake lock
  QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
  if ( activity.isValid() )
  {
    QAndroidJniObject serviceName = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Context","POWER_SERVICE");
    if ( serviceName.isValid() )
    {
      QAndroidJniObject powerMgr = activity.callObjectMethod("getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;",serviceName.object<jobject>());
      if ( powerMgr.isValid() )
      {
        jint levelAndFlags = QAndroidJniObject::getStaticField<jint>("android/os/PowerManager","SCREEN_DIM_WAKE_LOCK");

        QAndroidJniObject tag = QAndroidJniObject::fromString( "My Tag" );

        m_QAndroidJniObject_WakeLock = powerMgr.callObjectMethod("newWakeLock", "(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;", levelAndFlags,tag.object<jstring>());
      }
    }
  }
}

//-------------------------------------------------------------------------------------------------

void AndroidHelper::AcquireWakeLock()
{
  if ( m_QAndroidJniObject_WakeLock.isValid() == false)
  {
    qDebug() << "Invalid wake lock object";
    return;
  }

  m_QAndroidJniObject_WakeLock.callMethod<void>("acquire", "()V");
}

//-------------------------------------------------------------------------------------------------

void AndroidHelper::ReleaseWakeLock()
{
  if ( m_QAndroidJniObject_WakeLock.isValid() == false)
  {
    qDebug() << "Invalid wake lock object";
    return;
  }

  m_QAndroidJniObject_WakeLock.callMethod<void>("release", "()V");
}

//-------------------------------------------------------------------------------------------------
