#ifndef ANDROIDHELPER_H
#define ANDROIDHELPER_H

// Qt includes -----------------------------
#include <QAndroidJniObject>

//-------------------------------------------------------------------------------------------------

class AndroidHelper
{

public:

  AndroidHelper();

  void AcquireWakeLock();
  void ReleaseWakeLock();

private:

  QAndroidJniObject m_QAndroidJniObject_WakeLock;
};

#endif // ANDROIDHELPER_H
