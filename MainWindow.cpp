
// Files includes --------------------------
#include "MainWindow.h"
#include "ui_MainWindow.h"

// Project includes ------------------------
#ifdef Q_OS_ANDROID
  #include "AndroidHelper.h"
#endif

// Qt includes -----------------------------
#include <QDebug>
#include <QSound>

//-------------------------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , m_QSettings(QApplication::organizationDomain(),
                QApplication::applicationName())
  , m_AndroidHelper(nullptr)
  , m_TimerId(0)
  , m_State(State_Idle)
  , m_CurrentRepetition(0)
  , m_QElapsedTimer()
{
  ui->setupUi(this);

  QMainWindow::setWindowTitle(QApplication::applicationName());

#ifdef Q_OS_ANDROID
  m_AndroidHelper = new AndroidHelper();
#endif

  ui->m_QSpinBox_StretchDuration->setValue(m_QSettings.value("StretchDuration",
                                                             40).toInt());
  ui->m_QSpinBox_PauseDuration->setValue(m_QSettings.value("PauseDuration",
                                                           10).toInt());
  ui->m_QSpinBox_Repetitions->setValue(m_QSettings.value("Repetitions",
                                                         12).toInt());

  ui->m_QProgressBar_StretchDuration->setValue(0);
  ui->m_QProgressBar_StretchDuration->setMaximum(ui->m_QSpinBox_StretchDuration->value() * 1000);
  ui->m_QProgressBar_PauseDuration->setValue(0);
  ui->m_QProgressBar_PauseDuration->setMaximum(ui->m_QSpinBox_PauseDuration->value() * 1000);
  ui->m_QProgressBar_Repetitions->setValue(0);
  ui->m_QProgressBar_Repetitions->setMaximum(ui->m_QSpinBox_Repetitions->value());

  // Signals/slots
  QObject::connect(this,
                   SIGNAL(applicationStateChanged(Qt::ApplicationState)),
                   this,
                   SLOT(slot_QApplication_applicationStateChanged(Qt::ApplicationState)));
  QWidget::connect(this,
                   SIGNAL(signal_PlaySoundStart()),
                   this,
                   SLOT(slot_PlaySoundStart()));
  QWidget::connect(this,
                   SIGNAL(signal_PlaySoundStop()),
                   this,
                   SLOT(slot_PlaySoundStop()));
}

//-------------------------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
  m_QSettings.setValue("StretchDuration",
                       ui->m_QSpinBox_StretchDuration->value());
  m_QSettings.setValue("PauseDuration",
                       ui->m_QSpinBox_PauseDuration->value());
  m_QSettings.setValue("Repetitions",
                       ui->m_QSpinBox_Repetitions->value());

  #ifdef Q_OS_ANDROID
    m_AndroidHelper->ReleaseWakeLock();
    delete m_AndroidHelper;
  #endif

  delete ui;
}

//-------------------------------------------------------------------------------------------------

void MainWindow::timerEvent(QTimerEvent *event)
{
  Q_UNUSED(event)

  if(m_CurrentRepetition >= ui->m_QSpinBox_Repetitions->value())
    m_State = State_Finish;

  switch (m_State)
  {
  // State_Idle -------------------------------------------
  case State_Idle:
    // Do nothing
  break;

  // State_Start ------------------------------------------
  case State_Start:
  {
    ui->m_QLabel_State->setText(tr("Start"));
    ui->m_QProgressBar_StretchDuration->setValue(0);
    ui->m_QProgressBar_PauseDuration->setValue(0);
    ui->m_QProgressBar_Repetitions->setValue(0);

    m_CurrentRepetition = 0;
    m_QElapsedTimer.start();
    m_State = State_StartWait;
  }
  break;

  // State_StartWait --------------------------------------
  case State_StartWait:
  {
    double waitTimeS = (double)ui->m_QSpinBox_PauseDuration->value() - (double)m_QElapsedTimer.elapsed()/1000.0;
    ui->m_QLabel_State->setText(tr("Starting in %1").arg(QString::number(waitTimeS, 'f', 1)));

    if(m_QElapsedTimer.elapsed() >= ui->m_QSpinBox_PauseDuration->value()*1000)
      m_State = State_StretchStart;
  }
  break;

  // State_StretchStart -----------------------------------
  case State_StretchStart:
  {
    emit signal_PlaySoundStart();

    m_QElapsedTimer.start();
    m_State = State_StretchWait;
  }
  break;

  // State_StretchWait ------------------------------------
  case State_StretchWait:
  {
    double elapsedTimeS = (double)m_QElapsedTimer.elapsed()/1000.0;
    ui->m_QLabel_State->setText(tr("Stretch %1").arg(QString::number(elapsedTimeS, 'f', 1)));
    ui->m_QProgressBar_StretchDuration->setValue(m_QElapsedTimer.elapsed());
    ui->m_QProgressBar_StretchDuration->setFormat(QString("%1s").arg(QString::number(elapsedTimeS, 'f', 1)));

    if(m_QElapsedTimer.elapsed() >= ui->m_QSpinBox_StretchDuration->value()*1000)
    {
      m_CurrentRepetition++;

      ui->m_QProgressBar_Repetitions->setValue(m_CurrentRepetition + 1);

      if(m_CurrentRepetition >= ui->m_QSpinBox_Repetitions->value())
        m_State = State_Finish;
      else
        m_State = State_PauseStart;
    }
  }
  break;

  // State_PauseStart -------------------------------------
  case State_PauseStart:
  {
    emit signal_PlaySoundStop();

    m_QElapsedTimer.start();
    m_State = State_PauseWait;
  }
  break;

  // State_PauseWait --------------------------------------
  case State_PauseWait:
  {
    double elapsedTimeS = (double)m_QElapsedTimer.elapsed()/1000.0;
    ui->m_QLabel_State->setText(tr("Pause %1").arg(QString::number(elapsedTimeS, 'f', 1)));
    ui->m_QProgressBar_PauseDuration->setValue(m_QElapsedTimer.elapsed());
    ui->m_QProgressBar_PauseDuration->setFormat(QString("%1s").arg(QString::number(elapsedTimeS, 'f', 1)));

    if(m_QElapsedTimer.elapsed() >= ui->m_QSpinBox_PauseDuration->value()*1000)
      m_State = State_StretchStart;
  }
  break;

  // State_Finish -----------------------------------------
  case State_Finish:
  {
    emit signal_PlaySoundStop();

    killTimer(m_TimerId);
    m_State = State_Idle;
    ui->m_QPushButton_StartStop->setText(tr("Start"));
    ui->m_QLabel_State->setText(tr("Finish"));
    #ifdef Q_OS_ANDROID
      m_AndroidHelper->ReleaseWakeLock();
    #endif
  break;
  }
  }
}

//-------------------------------------------------------------------------------------------------

void MainWindow::slot_QApplication_applicationStateChanged(Qt::ApplicationState applicationState)
{
  if(   applicationState == Qt::ApplicationSuspended
     || applicationState == Qt::ApplicationHidden)
  {
    m_QSettings.sync();
  }
}

//-------------------------------------------------------------------------------------------------

void MainWindow::slot_PlaySoundStart()
{
  QSound::play(":/audio/start.wav");
}

//-------------------------------------------------------------------------------------------------

void MainWindow::slot_PlaySoundStop()
{
  QSound::play(":/audio/stop.wav");
}

//-------------------------------------------------------------------------------------------------

void MainWindow::on_m_QPushButton_StartStop_clicked()
{
  if(m_State != State_Idle)
  {
    killTimer(m_TimerId);
    m_State = State_Idle;

    ui->m_QPushButton_StartStop->setText(tr("Start"));
    ui->m_QLabel_State->setText(tr("Stopped"));

    #ifdef Q_OS_ANDROID
      m_AndroidHelper->ReleaseWakeLock();
    #endif

    return;
  }

  #ifdef Q_OS_ANDROID
    m_AndroidHelper->AcquireWakeLock();
  #endif

  m_State = State_Start;
  m_CurrentRepetition = 0;
  m_TimerId = startTimer(50);

  ui->m_QPushButton_StartStop->setText(tr("Stop"));
  ui->m_QLabel_State->setText(tr("Starting"));
}

//-------------------------------------------------------------------------------------------------

void MainWindow::on_m_QSpinBox_StretchDuration_valueChanged(int arg1)
{
  ui->m_QProgressBar_StretchDuration->setMaximum(arg1 * 1000);

  totalDurationChanged();
}

//-------------------------------------------------------------------------------------------------

void MainWindow::on_m_QSpinBox_PauseDuration_valueChanged(int arg1)
{
  ui->m_QProgressBar_PauseDuration->setMaximum(arg1 * 1000);

  totalDurationChanged();
}

//-------------------------------------------------------------------------------------------------

void MainWindow::on_m_QSpinBox_Repetitions_valueChanged(int arg1)
{
  ui->m_QProgressBar_Repetitions->setMaximum(arg1);

  totalDurationChanged();
}

//-------------------------------------------------------------------------------------------------

void MainWindow::totalDurationChanged()
{
  int totalDuration = (  ui->m_QSpinBox_PauseDuration->value()
                       + ui->m_QSpinBox_StretchDuration->value())
                      * ui->m_QSpinBox_Repetitions->value();

  ui->m_QLabel_State->setText(tr("Total duration %1m %2s").arg(totalDuration/60)
                                                          .arg(totalDuration%60));
}

//-------------------------------------------------------------------------------------------------
