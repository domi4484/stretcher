#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Qt includes -----------------------------
#include <QMainWindow>
#include <QElapsedTimer>
#include <QSettings>

// Forward declarations --------------------
namespace Ui { class MainWindow; }
class AndroidHelper;

//-------------------------------------------------------------------------------------------------

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

protected:

  void timerEvent(QTimerEvent *event);

signals:

  void signal_PlaySoundStart();
  void signal_PlaySoundStop();

private slots:

  void slot_QApplication_applicationStateChanged(Qt::ApplicationState applicationState);

  void slot_PlaySoundStart();
  void slot_PlaySoundStop();

  void on_m_QPushButton_StartStop_clicked();

  void on_m_QSpinBox_StretchDuration_valueChanged(int arg1);
  void on_m_QSpinBox_PauseDuration_valueChanged(int arg1);
  void on_m_QSpinBox_Repetitions_valueChanged(int arg1);

private:

  Ui::MainWindow *ui;

  QSettings m_QSettings;

  AndroidHelper *m_AndroidHelper;

  int m_TimerId;

  enum Enum_State
  {
    State_Idle,
    State_Start,
    State_StartWait,
    State_StretchStart,
    State_StretchWait,
    State_PauseStart,
    State_PauseWait,
    State_Finish
  };
  Enum_State m_State;

  int m_CurrentRepetition;
  QElapsedTimer m_QElapsedTimer;

  void totalDurationChanged();
};

#endif // MAINWINDOW_H
