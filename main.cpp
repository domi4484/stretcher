
// Project includes ------------------------
#include "MainWindow.h"

// Qt includes -----------------------------
#include <QApplication>

//-------------------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
  QApplication qApplication(argc, argv);

  // Application informations
  qApplication.setOrganizationName   ("Custom Cut KLG");
  qApplication.setOrganizationDomain ("customcut.ch");
  qApplication.setApplicationName    ("Stretcher");
  qApplication.setApplicationVersion ("V0.0.4");

  MainWindow mainWindow;
  mainWindow.show();

  return qApplication.exec();
}

//-------------------------------------------------------------------------------------------------
